import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        // Create an array
        int[] countArray = { 1, 2, 3, 4, 5 };

        // Create an ArrayList
        ArrayList<String> inventories = new ArrayList<>();
        inventories.add("Book");
        inventories.add("Pen");
        inventories.add("Bag");

        // Create a HashMap
        HashMap<String, Integer> countMap = new HashMap<>();
        countMap.put("KeyA", 123);
        countMap.put("KeyB", 456);
        countMap.put("KeyC", 789);

        // Iterate over array
        System.out.println("Iterating over Array:");
        for (int i = 0; i < countArray.length; i++) {
            System.out.println("Element at index " + i + ": " + countArray[i]);
        }

        //Iterate over Arraylist
        System.out.println("Iterating over ArrayList:");
        for (int i = 0; i < inventories.size(); i++) {
            String item = inventories.get(i);
            System.out.println("Element at index " + i + ": " + item);
        }

        //Iterate over HashMap
        System.out.println("Iterating over HashMap:");

        for (String key : countMap.keySet()) {
            Integer value = countMap.get(key);
            System.out.println("Key: " + key + ", Value: " + value);
        }













//        System.out.println("Iterating over Array:");
//        for(int count: countArray){
//            System.out.println(count);
//        }

//        // Iterate over ArrayList
//        System.out.println("Iterating over ArrayList:");
//        iterateCollection(arrayList, scanner);
//
//        // Iterate over HashMap
//        System.out.println("Iterating over HashMap:");
//        iterateMap(hashMap, scanner);


    }
}