import java.util.Scanner;

public class Main {
    public static void main(String[] args) {


        //While Loop
        System.out.println("Input an integer whose factorial will be computed:");

        Scanner in = new Scanner(System.in);
        int num = 0;

        try {
            num = in.nextInt();
        } catch (Exception e) {
            System.out.println("Invalid input. Please enter a valid integer.");
            System.exit(1);
        }

        int answer = 1;
        int counter = 1;

        if (num < 0) {
            System.out.println("Factorial is undefined for negative numbers.");
        } else if (num == 0) {
            System.out.println("Factorial of 0 is 1");
        } else {
            while (counter <= num) {
                answer *= counter;
                counter++;
            }
            System.out.println("The factorial of " + num + " is: " + answer);
        }


    }
}