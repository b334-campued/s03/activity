import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        //For Loop
        System.out.println("Enter an integer to compute its factorial:");

        Scanner in = new Scanner(System.in);
        int num = 0;

        try {
            num = in.nextInt();
        } catch (Exception e) {
            System.out.println("Invalid input. Please enter a valid integer.");
            System.exit(1);
        }

        int answer = 1;

        if (num < 0) {
            System.out.println("Factorial is undefined for negative numbers.");
        } else {
            for (int i = 1; i <= num; i++) {
                answer *= i;
            }

            System.out.println("The factorial of " + num + " is: " + answer);
        }
    }
}